enum CadinalDirection {
    North = "North",
    East = "East",
    South = "South",
    West = "West"
}

let currentDirection = CadinalDirection.East

console.log(currentDirection)