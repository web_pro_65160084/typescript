let w:unknown = 1;
w = "string";
w = {
    runANonExistenceMethod: () => {
        console.log("I think therefore i am")
    } 
} as { runANonExistenceMethod: () => void }

if(typeof w === 'object' && w !== null){
    (w as {runANonExistenceMethod: () => void}).runANonExistenceMethod();
}